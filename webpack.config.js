var path = require("path");

module.exports = {
  devtool: "#inline-source-map",
  entry: path.join(__dirname, "src", "index.js"),
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist'),
  },
  devServer: {
    contentBase: "static/",
  },
   resolve: {
    alias: {
      'handlebars' : 'handlebars/dist/handlebars.js'
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, "src"),
        loader: "babel-loader"
      },
      {
        test: /\.handlebars$/,
        include: path.join(__dirname, "src", "components", "templates"),
        loader: "handlebars-loader?helperDirs[]=" + path.join(__dirname, "src", "helpers")
      }
    ]
  }
}