This project is written using ES6, compiled via Babel and packaged with Webpack. To build it, take the following measures:

  1. Make sure you have recent node.js and npm installed.
  2. Check out this project.
  3. CD into this project's folder.
  4. type `npm install` and let it install all of the remote dependencies.
  5. **CRUCIAL** - this project depends on a module called `deque-widgets` (https://bitbucket.org/dmusser/aria-library), which is not available via NPM yet. In order to make this work, you _must_ also download the deque-widgets projects into a separate folder somewhere. Then, type `cd <widgets project folder> && npm install`, then  `npm run buildAsLocalDep`. This will make `deque-widgets` available to npm on your computer. (note, depending on your node configuration that last step may require root access).
  6. type `npm link deque-widgets` to pull the widgets project into this project.
  7. type `npm start` to run a local version of this project. You can then see it run at http://localhost:8080
  8. type `npm run build` to generate a final build. You'll find it in a folder called `dist` within this project - the dist folder is ready to be deployed anywhere, and hosted statically as a stand-alone app. You can also pull out its js, css and html and stick them into your own template somewhere.

To integrate this into your own context, you'll want to be aware of two things:

  1. Call `window.initializeExercises(exercises, index)` to kick it off. If `window.dqu` doesn't exist when this is added to the DOM it'll automatically initialize itself using hard-coded mock data, so make sure you add the dqu library before you add this.

  2. Every time someone gets a question right, it sees if it can call the following function: `window.dqu.exercise_passed(currentIndex);` (if window.dqu doesn't exist, or if this function isn't on it, it's just a no-op).

## Additional Notes ##
When editing code in full-screen mode, the text areas have a 550px minimum width - if the screen is narrower than that (i.e. on a mobile device, but also on a downsized window) then they'll stack vertically instead of rendering side by side, as requested.

I want to get the deque-widgets module published sooner rather than later so that we don't have this wonky bespoke build process.