export default function helperItemIterator(items, template) {
  var out = '';
  if (items) {
    for (var i = 0; i < items.length; i++) {
      out += template(items[i]);
    }
  }
  return out;
}