/*

wcag2a = WCAG 2.0 Level A
wcag332 = WCAG 2.0 Success Criterion 3.3.2
section508 = Section 508
section508.22.n = Section 508 1194.22(n)

*/

function parseWCAG(tag) {
  var retVal = 'WCAG 2.0';
  tag = tag.substr(4);
  if (tag[0] === "2") {
    tag = tag.substr(1);
  }

  if (!isNaN(parseInt(tag))) {
    var parts = tag.split('');
    while (parts.length > 3) {
      parts[2] = parts[2] + "" + parts[3];
    }
    retVal += " Success Criterion " + parts.join('.');
  } else {
    retVal += " Level " + tag.toUpperCase();
  }

  return retVal;
}

function parseSection508(tag) {
  var retVal = "Section 508";
  if (tag === "section508") {
    return retVal;
  }

  var parts = tag.split('.');
  retVal += " 1194." + parts[1] + "(" + parts[2] + ")"
  return retVal;
}

export default {
  expand: function(tag) {
    if (tag.indexOf('wcag') === 0) {
      return parseWCAG(tag);
    }

    if (tag.indexOf('section508') === 0) {
      return parseSection508(tag);
    }
    return tag;
  }
}