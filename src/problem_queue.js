export default function initializeProblemQueue(problems) {
  var currentIndex = 0;
  problems = problems.map((item, index) => {
    item.index = index;
    return item;
  })

  return {
    pass: function() {
      if (window.dqu) {
        window.dqu.exercise_passed(currentIndex);
      }

      currentIndex++;
    },
    next: function() {
      return problems[currentIndex] || null;
    },
    setIndex: function(i) {
      currentIndex = i;
    }
  }
}