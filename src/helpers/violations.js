import compiledNavItemTemplate from '../components/templates/navItemTemplate.handlebars';
import helperItemIterator from '../utils/helperItemIterator';

export default function(items) {
  return helperItemIterator(items, compiledNavItemTemplate);
}