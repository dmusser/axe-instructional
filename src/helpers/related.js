import compiledRelatedNodeTemplate from '../components/templates/relatedNodeTemplate.handlebars';
import helperItemIterator from '../utils/helperItemIterator';

export default function(items) {
  return helperItemIterator(items, compiledRelatedNodeTemplate);
}