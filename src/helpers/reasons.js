import compiledFailureTemplate from '../components/templates/failureTemplate.handlebars';
import helperItemIterator from '../utils/helperItemIterator';

export default function(items) {
  return helperItemIterator(items, compiledFailureTemplate);
}