import axe from 'axe-core';
import initializeEditor from './components/code_editor.js';
import initializeDisplay from './components/code_display.js';
import initializeDialogBox from './components/dialog_box.js';
import initializeProblemQueue from './problem_queue.js';
import initializeValidator from './validator';

var dialogBox = initializeDialogBox('dialogBox');
var problemQueue;

var evalButtonText = {
  ready: "I'm Done! Evaluate the Code!",
  pending: "Processing Changes..."
};

var evalButton = document.getElementById('evaluate');
var fullscreenButton = document.getElementById('goFullScreen');
var header = document.getElementById('axe_header');
var instructionText = document.getElementById('instructionText');
let exercises = [];
var failureResults;

function showSuccessNotification(message, revertToFullscreen) {
  dialogBox.showSuccessNotification(message, evalButton, (e) => {
    renderNextQuestion();
    if (revertToFullscreen) {
      showFullscreen();
    } else {
      header.focus();
    }

  });
}

function showFailureNotification(message, revertToFullscreen) {
  dialogBox.showFailureNotification(message, editor, function() {
    showFailureDetails({target: evalButton});
  }, () => {
    if (revertToFullscreen) {
      showFullscreen();
    }
  });
}

var rerenderTimeout;
var editor = initializeEditor(document.getElementById('htmlINPUT'));
editor.onInput(() => {
  if (rerenderTimeout) {
    clearTimeout(rerenderTimeout);
  }

  evalButton.innerText = evalButtonText.pending;

  evalButton.setAttribute('disabled', '');

  rerenderTimeout = setTimeout(function() {
    renderInput(function(e) {
      rerenderTimeout = null;
      evalButton.removeAttribute('disabled');
      evalButton.innerText = evalButtonText.ready;
    });

  }, 1000);

});

var display = initializeDisplay(document.querySelector('iframe.render'));

function onPass(revertToFullscreen) {
  problemQueue.pass();
  showSuccessNotification("Congratulations, you've fixed all of the problems.", revertToFullscreen);
}

function onFail(results, revertToFullscreen) {
  failureResults = results;
  var summary = document.querySelector('.fail .summary');
  var message;

  if (results.violations.length > 1) {
    message = `There are still ${results.violations.length} problems with this markup.`;
  } else {
    message = "There is still 1 problem left with this markup.";
  }

  showFailureNotification(message, revertToFullscreen);
}

var validator = initializeValidator();

var current;
function renderNextQuestion() {
  current = problemQueue.next();

  if (!current) { return finish(); }

  updateInstructions(current.instructions);
  updateHeader(current.index);
  updateInput(current.input);
}

function finish() {
  header.innerText = `Congratulations you've fixed all ${exercises.length} problems!`;
  document.getElementById('instructions').classList.add('hidden');
  document.getElementById('quizArea').classList.add('hidden');
  header.focus();
}

function updateInstructions(instructions) {
  instructionText.innerText = instructions;
}

function updateHeader(index) {
  header.innerText = `Exercise ${index + 1} of ${exercises.length}`;
}

function updateInput(question) {
  editor.setValue(question);
  renderInput();
}

function renderInput(callback) {
  display.setContent(editor.getValue(), current.type, callback);
}

function showFailureDetails(e) {
  dialogBox.showViolationDetails(e.target, failureResults.violations);
}

function tryToValidate(input, revertToFullscreen) {
  let iframe = document.querySelector('iframe');

  if (!iframe || iframe.contentDocument.querySelector('script') === null) {
    setTimeout(tryToValidate.bind(null, input, revertToFullscreen), 100);
  } else {
    validator.validate(input, current.type, current.mustInclude, (results) => {
      if (results.violations.length === 0) {
        onPass(revertToFullscreen);
      } else {
        onFail(results, revertToFullscreen);
      }
    });
  }
}

function evaluateInput(input, revertToFullscreen) {
  failureResults = null;
  // always try to do one more render, just in case.
  renderInput(() => {
    tryToValidate(input, revertToFullscreen);
  });
}

function showFullscreen() {
  dialogBox.showFullScreen(fullscreenButton, function() {
    evaluateInput(editor.getValue(), true);
  });
}

evalButton.addEventListener('click', function() {
  evaluateInput(editor.getValue());
});

fullscreenButton.addEventListener('click', showFullscreen);

window.initializeExercises = function(questions, index) {
  exercises = questions;
  problemQueue = initializeProblemQueue(exercises);
  problemQueue.setIndex(index);
  renderNextQuestion();
}