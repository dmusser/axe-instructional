export default function initializeValidator() {

  function confirmIncludes(answer, mustInclude) {
    if (!mustInclude || mustInclude.lenth === 0) {
      return true;
    }

    return mustInclude.reduce((acc, val) => {
      if (!acc) { return false; }

      return answer.indexOf(val) > -1;
    }, true);
  }

  return {
    validate: function(answer, type, mustInclude, callback) {
      if (!confirmIncludes(answer, mustInclude)) {
        var requiredFields = mustInclude.join(", ");
        return alert("Your answer must include the following items: " + requiredFields);
      };

      var query = (type === 'node') ? '#testElement' : 'html';
      axe.a11yCheck({
        include: [['#renderFrame', query]]
      }, {
        rules: { 'heading-order': { enabled: true }}
      }, (result) => {
        console.log(result)
        callback(result)
      });
    }
  }
}