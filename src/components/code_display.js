const templates = {
  reset: function() {
    return `<!DOCTYPE html><html lang="en-us"><head><title>ARIA Quiz</title><script type="text/javascript" src="${pathPrefix}axe.min.js"></script></head><body><div id="testElement"></div></body></body>`
  },
  node: function(content) {
    content = content.split(`'`).join(`\\'`);
    return `<!DOCTYPE html><html lang="en-us"><head><title>ARIA Quiz</title><script type="text/javascript" src="${pathPrefix}axe.min.js"></script></head><body><div id="testElement">${content}</div></body></body>`
  },
  document: function(content) {
    content = content.split(`'`).join(`\\'`);

    if (content.indexOf("</head>") > -1) {
      content = content.split("</head>").join(`<script type="text/javascript" src="${pathPrefix}axe.min.js"></script></head>`);
    } else if (content.indexOf('</body>') > -1) {
      content = content.split('</body>').join(`<script type="text/javascript" src="${pathPrefix}axe.min.js"></script></body>`);
    } else {
      content += `<script type="text/javascript" src="${pathPrefix}axe.min.js"></script>`;
    }

    return content;
  }
}

export default function initializeDisplay(iframe) {
  function getContent(iframe) {
    return oldURL.substr(13).slice(0, -1)
  }

  let oldURL;

  // this is to handle those times when the iframe is not fully rendered
  // (i.e. because it was moved from dialog back to fullscreen or vice versa)
  // but some request has come in expecting it to work. We hold onto that callback
  // and fire it once the whole thing is ready.
  var awaiting = [];
  iframe.addEventListener('load', () => {
    while(awaiting.length > 0) {
      awaiting.pop()(getContent(iframe));
    }
  });

  iframe.refreshContent = function() {
    iframe.contentWindow.location.href = "";
    iframe.contentWindow.location.href = oldURL;
  };

  return {
    setContent: function(content, type, callback) {
      var doc = templates[type](content);
      var newURL = `javascript: '${doc}'`;

      if (newURL !== oldURL) {
        // only load if content changed.
        oldURL = newURL;
        if (callback) {
          awaiting.push(callback);
        }

        iframe.contentWindow.location.href = newURL;
      } else if (callback) {
        if (iframe.contentDocument.readyState  !== 'complete') {
          awaiting.push(callback);
        } else {
          callback(getContent(iframe));
        }
      }
    }
  }
}