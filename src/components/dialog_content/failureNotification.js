export function getDetails(element, showViolationDetails, editAgain) {
  return {
    message: element,
    title: "Incorrect!",
    showHeader: true,
    classes: ['incorrect'],
    buttons: [
      {
        label: "Show Accessibility Issue Details",
        postClose: showViolationDetails
      },
      {
        label: "Make Additional Edits",
        postClose: editAgain,
        handleEscape: true
      }
    ]
  }
}

export function show(showDialog, message, trigger, showViolationDetails, editAgain) {
  var details = getDetails(message, showViolationDetails, editAgain);
  showDialog(trigger, details);
}