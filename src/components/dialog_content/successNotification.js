export function getDetails(element, nextExercise) {
  return {
    message: element,
    title: "Correct!",
    showHeader: true,
    classes: ['correct'],
    buttons: [
      {
        label: "Next Exercise",
        postClose: nextExercise,
        handleEscape: true
      }
    ]
  }
}

export function show(showDialog, message, trigger, nextExercise) {
  var details = getDetails(message, nextExercise);
  showDialog(trigger, details);
}