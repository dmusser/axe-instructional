export function getDetails(element, evaluate, cancelFullScreen) {
  return {
    message: element,
    title: "Fix the accessibility errors in the text input below.",
    showHeader: true,
    classes: ['fullscreen'],
    lifecycle: {
      postOpen: function() {
        document.querySelector("iframe").refreshContent();
      },
      postClose: function() {
        document.querySelector("iframe").refreshContent();
      }
    },
    buttons: [
      {
        label: "I'm Done! Evaluate the Code!",
        preClose: cancelFullScreen,
        postClose: evaluate
      },
      {
        markup: `<button><img src="${pathPrefix}fullscreen-icon-reverse.png" width="18" height="18" alt="cancel full screen" />Cancel Full Screen</button>`,
        preClose: cancelFullScreen,
        handleEscape: true
      }
    ]
  }
}

export function show(showDialog, element, trigger, evaluate) {
  var details = getDetails(element, evaluate, function() {
    var quizWrapper = document.getElementById('inputHolder').appendChild(element);
  });

  showDialog(trigger, details);
}