import compiledNavTemplate from '../templates/navTemplate.handlebars';
import compiledRelatedListTemplate from '../templates/relatedListTemplate.handlebars';
import compiledReasonsTemplate from '../templates/reasonsTemplate.handlebars';
import tagExpander from '../../utils/tagExpander';

function messageFromRelatedNodes(relatedNodes) {
  var retVal = '';
  if (relatedNodes.length) {
    var list = relatedNodes.map(function(node) {
      return {
        targetArrayString: JSON.stringify(node.target),
        targetString: node.target.join(' ')
      };
    });
    retVal += compiledRelatedListTemplate({
      relatedNodeList: list
    });
  }
  return retVal;
}

function messagesFromArray(nodes) {
  var list = nodes.map(function(failure) {
    return {
      message: failure.message,
      relatedNodesMessage: messageFromRelatedNodes(failure.relatedNodes)
    }
  });
  return compiledReasonsTemplate({
    reasonsList: list
  });
}

function summarizeNode(node) {
  var retVal = '';
  if (node.any.length) {
    retVal += '<h3 class="error-title">Fix any of the following</h3>';
    retVal += messagesFromArray(node.any);
  }

  var all = node.all.concat(node.none);
  if (all.length) {
    retVal += '<h3 class="error-title">Fix all of the following</h3>';
    retVal += messagesFromArray(all);
  }
  return retVal;
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}


function buildDetailView(violation) {
  var tags = violation.tags.map(tag => "<li>" + tagExpander.expand(tag) + "</li>");

  var description = violation.description;
  if (description.indexOf('Ensures') === 0) {
    description = "This rule ensures " + description.substr(7);
  }

  var retVal = '<h2>Accessibility Error Details</h2>';
  retVal += '<h3>' + escapeHtml(description) + '</h3>';
  retVal += "Relevant Guidelines: <ul>";
  retVal += tags.join('');
  retVal += "</ul>";

  retVal += summarizeNode(violation.nodes[0]);

  return retVal;
}

function buildPrettyViolationList(violations) {
  var html = '';
  if (violations.length) {
    var localViolations = violations.map(function(rule, index) {
      const count = rule.nodes.length;
      const impact = rule.impact;
      const help = rule.help;
      const details = ("" + count + " " + impact + " ") + ((count === 1) ? "accessibility issue" : "accessibility issues");
      return {
        impact,
        count,
        help,
        index,
        details
      };
    });

    html = compiledNavTemplate({
      instructions: localViolations.length > 1 ? "Select a button below to show accessibility error details." : null,
      violationList: localViolations
    });
  }

  return html;
}

export function getDetails(element, clearContent) {
  return {
    message: element,
    title: "Accessibility Issue Summary",
    showHeader: true,
    classes: ['issuedetails'],
    buttons: [
      {label: "Close", preClose: clearContent, handleEscape: true}
    ]
  }
};

export function show(showDialog, element, trigger, violations) {
  var listElement = element.querySelector('.violationList');
  var detailElement = element.querySelector('.violationDetail');

  listElement.innerHTML = buildPrettyViolationList(violations);
  detailElement.innerHTML = buildDetailView(violations[0]);
  var links = listElement.querySelectorAll('.violationNavItem');

  function setActiveLink(index) {
    for (var i = 0; i < links.length; i++) {
      var link = links[i];
      var linkIndex = link.dataset.index;

      if (linkIndex === index) {
        link.classList.add('active');
      } else {
        link.classList.remove('active');
      }
    }
  }

  function selectNavItem(index) {
    setActiveLink(index);
    detailElement.innerHTML = buildDetailView(violations[index]);
  }


  for (var i = 0; i < links.length; i++) {
    links[i].addEventListener('click', (e) => selectNavItem(e.target.dataset.index));
    links[i].addEventListener('keypress', (e) => {
      e.stopPropagation();

      if (e.which === 13) {
        selectNavItem(e.target.dataset.index);
      }
    });
  }

  selectNavItem("0");

  var violationDetails = getDetails(element, function() {
    listElement.innerHTML = "";
    detailElement.innerHTML = "";
  });

  showDialog(trigger, violationDetails);
}