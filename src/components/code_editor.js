export default function initializeEditor(element) {

  element.addEventListener("keypress", (e) => {
    if (e.which === 13) { // keep 'Enter' from closing dialog window
      e.stopPropagation();
    }
  })

  return {
    getValue: function() {
      return element.value;
    },
    setValue: function(text) {
      element.value = text;
    },
    onChange: function(callback) {
      element.addEventListener('change', callback)
    },
    onInput: function(callback) {
      element.addEventListener('input', callback);
    },
    focus: function() {
      element.focus();
    }
  }
}