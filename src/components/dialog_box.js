import {createDialog} from 'deque-widgets';

import * as successNotification from './dialog_content/successNotification';
import * as failureNotification from './dialog_content/failureNotification';
import * as violationDetail from './dialog_content/violationDetail';
import * as fullScreen from './dialog_content/fullScreen';

export default function initializeDialogBox(id) {
  var showDialog = createDialog(id);

  var violationElement = document.getElementById('detailContent');
  var exerciseInput = document.getElementById('inputWrapper');

  return {
    showFullScreen: fullScreen.show.bind(null, showDialog, exerciseInput),
    showViolationDetails: violationDetail.show.bind(null, showDialog, violationElement),
    showSuccessNotification: successNotification.show.bind(null, showDialog),
    showFailureNotification: failureNotification.show.bind(null, showDialog)
  }
}